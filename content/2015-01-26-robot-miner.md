+++
title = "Как собрать шахтерского робота "
description = '''
Абстрактное описание сборки робота я уже писал много раз, поэтому это
будет короткое руководство на конкретном примере.

Соберем и запустим [Totoro Recursive Miner](http://computercraft.ru/topic/207-totoro-recursive-miner-opencomputers/?do=findComment&comment=5657).
'''

[taxonomies]
authors = ["Totoro"]
categories = ["opencomputers"]
tags = ["opencomputers", "lua", "роботы", "шахтер"]
+++

## Как собрать робота
**в 5 шагов**  
*(инструкция для самых маленьких)*

![](https://lh6.googleusercontent.com/-vlJBrUwFZg4/VMWJWTJmA8I/AAAAAAAAAoc/jv-Hx7lTWhQ/w925-h530-no/2015-01-26_00.44.47.png)

Абстрактное описание сборки робота я уже писал много раз, поэтому это
будет короткое руководство на конкретном примере.

Соберем и запустим [Totoro Recursive Miner](http://computercraft.ru/topic/207-totoro-recursive-miner-opencomputers/?do=findComment&comment=5657).

### Шаг 1. Подготовка
![](https://ocdoc.cil.li/_media/blocks:assembler.png?w=128&tok=95e838)

Для создания робота нам потребуется сборщик (assembler).

Чтобы он работал - подведите питание. Сборка робота потребует некоторого
времени (примерно 5 минут) и энергозатрат.

### Шаг 2. Подбор железа
Запчасти делятся на обязательные и необязательные.

Детали обязательные:
1) **Корпус**. Основа робота. Без него никуда. Для TRMiner нужен корпус 2+
уровня, потому что он должен содержать апгрейд-генератор.
2) **Процессор**. Мощность процессора определяет количество выполняемых
роботом операций в такт. Т.е. проще говоря - скорость его работы.  
Однако перемещаться быстрее робот не станет. Этот параметр можно улучшить
"прокачав" робота (см. апгрейд-опыт).
3) **Память**. Практика показала, что одной планки 1 уровня для нормальной
работы на компьютере недостаточно. TRMiner хранит в памяти данные о жилах
руды, поэтому требует как минимум две планки уровня 1.5. При меньшем
количестве корректную работу не гарантирую. (Хотя возможно он будет работать.)
4) **Монитор**. Достаточно 1 уровня. Робот не поддерживает цветные
экраны. (Можно собрать робота и без экрана. Но это - для любителей хардкора.)
5) **Видеокарта**. Также достаточно 1-го уровня. Требуется для вывода
изображения на монитор. Без нее монитор будет просто черным.
6) **Клавиатура**. Чтобы иметь возможность набрать что-то в консоли.
7) **Дисковод**. Для установки OpenOS и копирования программы TRMiner.
(Любители хардкора могут попробовать запустить робота без дисковода.
Это возможно. Но я не скажу как :P )
8) **Жесткий диск**. Для хранения ОСи и программы. Первоэтапный диск
в 1Мб хватит с головой. Это даже много. Будет занято ~20%.
9) **Lua BIOS**. Этот чип нужен для корректной работы OpenOS. Крафтится
из пустого EERPOM и книги.

Детали обязательные для Totoro Recursive Miner:
10) **Апгрейд-инвентарь**. Робот хранит в нем добытую руду. Рекомендуется
установить 2 или 3 апгрейда (т.е. 32 или 48 слотов). Больше можно не
ставить, ибо обычный сундук, в который робот сбрасывает добычу имеет
размер в 27 слотов.
11) **Апгрейд-генератор**. Нужен роботу для непрерывной работы. Робот
будет сам заряжаться с его помощью, сжигая часть добытого угля.  
(Любители хардкора могу не ставить генератор. Программа будет работать.
Вы можете заряжать робот таская за ним заряжающее устройство, или
приделав пару солнечных панелей и выкопав вертикальный колодец до
поверхности. :D)

Детали необязательные:
12) **Апгрейд-опыт**. Позволит роботу прокачиваться во время
добычи. Со временем он станет быстрее двигаться, меньше тратить
энергию и медленнее ломать свой инструмент. Требует Корпуса 3-его уровня.
13) **Апгрейд-батарея**. Ну тут все понятно. Увеличивает емкость
аккумулятора. Полезная штука.

Детали вредные (эксклюзив для сервера `IT 1.7.10`):
14) **Апгрейд-чанклоадер**. В силу того, как сконфигурирован сервер, после
включения робота в момент опустошит его аккумулятор. На том все и закончится.

Вот две рабочие конфигурации:

![](https://lh3.googleusercontent.com/-WgXpWoLqwHw/VMWJbdcya4I/AAAAAAAAApM/997-8OPoKa8/w352-h384-no/minimal.png)  
*Минимальная*

![](https://lh5.googleusercontent.com/--GxCw0D_5E0/VMWJbiRpUsI/AAAAAAAAApI/R35OsAtOrJs/w352-h384-no/recommended.png)  
*Рекомендуемая*

*(UPD.: Тут уважаемый Krutoy любезно предоставил картинку, которая иллюстрирует,
сколько всего ресурсов у вас уйдет на сборку рекомендуемой конфигурации робота:  
![](https://i.imgur.com/Sax0oS4.png)  
За что ему большое спасибо.)*

Уложите выбранные детали в сборщик и запускайте процесс.

### Шаг 3. Софт
Раздобудьте дискету с OpenOS (крафтится из чистой дискеты и книги).

Скачайте программу Totoro Recursive Miner на другую, чистую дискету.

[http://pastebin.com/L21VMm7S](http://pastebin.com/L21VMm7S)

Для этого этапа нам потребуется компьютер. Свой или соседа, все равно.
Он должен иметь выход в интернет (интернет-плата) и дисковод для дискет.

#### Как скачать программу на новую дискету:
* Вставить дискету
* Посмотреть в инвентаре ее адрес. Запомнить первые его буквы-цифры.
* Написать в консоли команду:

```bash
label -a xxxx floppy
```

Где `xxxx` - первые буквы-цифры ее адреса, а `floppy` - это будущее
название (этикетка).

В результате ваша дискета получит короткое и ясное название.

* Написать команды:

```bash
mount floppy fcd /f
```

В результате вы окажетесь в корневом каталоге дискеты.

* Скачать программу TRMiner:

```bash
pastebin get L21VMm7S mine
```

Для этого нужна интернет-плата. Программа будет сохранена на дискету
под именем `mine`.

* Извлечь дискету.

(Также можно поискать игрока с ником Totoro и подоставать его, чтобы
дал дискету с программой нахаляву. Тогда и компьютер не нужен.)

![](https://lh5.googleusercontent.com/-kbDO6DUWGlk/VMWJb2ZtxxI/AAAAAAAAApQ/AbYOlOPnLxQ/w169-h305-no/totoro.png)

### Шаг 4. Установка
Поставьте робота. Можно прямо на месте предполагаемой добычи руды.
Чтоб два раза не ходить.

Включите его и установите OpenOS (это надо сделать только один раз).

#### Как установить OpenOS:
1) Вставить в робота зеленую дискету.
2) Включить его.
3) Написать в консоли: `install`
4) Он спросит на какой жесткий диск устанавливать. Напишите `1`.
5) Согласитесь на рестарт (`y`).

Теперь сбросьте программу TRMiner с дискеты на жесткий диск робота.
(Можно каждый раз вставлять дискету и запускать программу прямо с нее,
но это лишние действия. Зачем оно нам?)

#### Как сбросить программу с дискеты:
1) Вставить дискету с программой в робота.
2) Убедиться, что он включен.
3) Написать в консоли робота: `mount floppy fcp f/mine mine`
4) Достать дискету.

Все! Софт установлен. Дискеты больше в принципе не нужны. Но сохраните
их на всякий случай.

### Шаг 4A. Настройка программы *(необязательно)*
Для настройки программы Totoro Recursive Miner, введите в консоль команду:

```bash
edit mine
```

В двадцатой строке вы увидите константы набранные заглавными буквами:

```lua
TECH_SLOTS = 6
VANILLA_CHEST = true
PATHWAYS = true
DROP_TRASH = false
```

`TECH_SLOTS` - количество слотов с образцами "пустой породы" и сундуками.
То есть тех слотов, которые не будут заняты добычей.

`VANILLA_CHEST` - режим для работы с обычными сундуками. Есть возможность
работать с сундуком Эндера.

Для этого, установите константу в значение `false` и дайте роботу
инструмент с зачарованием "Шелковое касание". В слот с сундуками
положите один сундук Эндера.

`PATHWAYS` - если `true`, робот проделает в шахте дорожки, для удобства
хождения игрока

`DROP_TRASH` - если true, робот будет выбрасывать булыжник и другую
"пустую породу".

После изменения констант, нажмите клавиши `Ctrl`+`S` (сохранение) и
`Ctrl`+`W` (выход).

### Шаг 5. Добыча полезных ископаемых
Принесите робота на место предполагаемой шахты. Поставьте робота в ее
воображаемый правый передний угол, передней стороной вперед.

Вот так:

![](https://lh6.googleusercontent.com/-2CR7UW8GB7w/VMWJbr29LqI/AAAAAAAAApU/WIYI7-PI7ug/w925-h530-no/mine_plan.png)

В инвентаре робота разложите образцы пустой породы (5 штук по дефолту).
Причем (лайфхак для ускорения работы робота), кладите в порядке убывания
распространенности. У меня это камень-земля-гравий-булыжник-камень Бездны
(abyssal stone из RailCraft). В последний из технических слотов
(6-ой по дефолту) положите сундуки (или сундук Эндера, если вы перенастроили
программу).

Роботу в "руку" положите кирку или бур. Чем прочнее и острее - тем лучше.

![](https://lh5.googleusercontent.com/-YNDV2KUf0lo/VMWJY6jKDSI/AAAAAAAAAo0/yibiKagZ8ro/s512-no/items.png)

Теперь включите. Введите в консоль команду такого формата:

```bash
mine <длина> [ширина] [возвращаться_в_начало]
```

Первые два параметра - числовые. Последний - `true`/`false` (Если не указать,
равен `false`).

![](https://lh5.googleusercontent.com/-fvxtcoQiqQ4/VMWJYexZDvI/AAAAAAAAAos/bTRSdr6DyP4/w925-h530-no/2015-01-25_23.34.51.png)

Ура! Наконец все ездит, копает и складывает без нашего участия. Остается
только иногда менять кирку. И уносить добычу.

![](https://lh3.googleusercontent.com/-LJEdHqxoVtI/VMWJVfRr6aI/AAAAAAAAAoU/A7va0o2t3jU/w925-h530-no/2015-01-26_00.00.17.png)

Enjoy!
